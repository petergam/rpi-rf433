var exec = require('child_process').exec
var path = require('path')
var Mutex = require( 'Mutex' )

module.exports = new RF433()

function RF433() {}

RF433.prototype.sendCode = function (code, options, callback) {
    var mutex = new Mutex('RF433_mutex')
    mutex.waitLock(function (err) {
        exec([path.join(__dirname, 'Tools/codesend'),
        code,
        options.protocol
      ].join(' '), function (error, stdout, stderr) {
        error = error || stderr    
        callback(error, stdout)
        mutex.unlock()
      })
    })
}
