var RF433 = require('./index')

var args = process.argv.slice(2);
if (args.length != 2) {
    console.log('Usage: RF433 code protocol')
    process.exit()
}

RF433.sendCode(args[0], {
    protocol: args[1]
}, function(err, output) {
    if (err) { console.log(err) }
    else { console.log(output) }
})
